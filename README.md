latexpand
=========

latexpand is a tool to flatten LaTeX file by expanding \include and
\input, ... and optionally remove comments.

The most common use of latexpand is to simplify distribution of source
LaTeX files, typically to satisfy the requirement of editors and
archival sites (springer, arXiv.org, ...) who force the authors to
submit sources. One does not necessarily want to submit sources with
comments, and uploading a document made of several files including
each other is a bit painful. By default, latexpand answers both
problems by outputing a single LaTeX file that contains no comment.

For more details, run `latexpand --help` or see the documentation at
the end of [latexpand](latexpand).
